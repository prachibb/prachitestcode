<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
		function validate_required(field,alerttxt)
			{
			with (field)
			  {
			  if (value==null||value=="")
				{
				alert(alerttxt);return false;
				}
			  else
				{
				return true;
				}
			  }
			}
			
		function testcall(){

			with (testform)
			  {
			  if (validate_required(ozipcode,"Origin Zip Code is required")==false)
			  {ozipcode.focus();return false;}			
			  if (validate_required(dzipcode,"Destination Zip Code is required")==false)
			  {dzipcode.focus();return false;}
			   if (validate_required(wght,"Weight is required")==false)
			  {wght.focus();return false;}
			  if (validate_required(cog,"Cost of Good is required")==false)
			  {cog.focus();return false;}
			  if (validate_required(wdth,"Width is required")==false)			
			  {wdth.focus();return false;}
			  if (validate_required(hgt,"Height is required")==false)
			  {hgt.focus();return false;}
			  if (validate_required(leng,"Length is required")==false)
			  {leng.focus();return false;}	
			  
			  ajaxtestcall();
			}
		
		}
		
				var xmlhttp;

				function ajaxtestcall()
				{
				
				xmlhttp=GetXmlHttpObject();
				if (xmlhttp==null)
				  {
				  alert ("Browser does not support HTTP Request");
				  return;
				  }
				var url="prachitest1.php";      
				xmlhttp.onreadystatechange=ajaxfinalChanged;
				xmlhttp.open("POST",url,true);
				xmlhttp.send();
				}

				function ajaxfinalChanged()
				{
					if (xmlhttp.readyState==4)
					{
						document.getElementById("ajaxtable").innerHTML=xmlhttp.responseText;
					}
				}

				   
				function GetXmlHttpObject()
				{
				if (window.XMLHttpRequest)
				  {
				  // code for IE7+, Firefox, Chrome, Opera, Safari
				  return new XMLHttpRequest();
				  }
				if (window.ActiveXObject)
				  {
				  // code for IE6, IE5
				  return new ActiveXObject("Microsoft.XMLHTTP");
				  }
				return null;
				}
						
	</script>
</head>
<body>
	<form method="post" name="testform" id="testform">
		<table>
			<tr>
				<td>Origin Zip Code :</td>
				<td><input type="text" name="ozipcode" id="ozipcode" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td>Destination Zip Code :</td>
				<td><input type="text" name="dzipcode" id="dzipcode" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td>Weight :</td>
				<td><input type="text" name="wght" id="wght" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td>Cost Of Goods :</td>
				<td><input type="text" name="cog" id="cog" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td>Width :</td>
				<td><input type="text" name="wdth" id="wdth" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td>Height :</td>
				<td><input type="text" name="hgt" id="hgt" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td>Length :</td>
				<td><input type="text" name="leng" id="leng" /><font color="red">*</font></td>
			</tr>
			<tr>
				<td> </td>
				<td><input type="button" name="buttoncal" value="Calculate" onclick="testcall();"/></td>     
			</tr>
		</table>
	</form>
		<div id="ajaxtable">
		<table width="1">
				<tr>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['ozipcode']; ?></td>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['dzipcode']; ?></td>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['wght']; ?></td>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['cog']; ?></td>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['wdth']; ?></td>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['hgt']; ?></td>
					<td><?php if(isset($_POST['buttoncal'])) echo $_POST['leng']; ?></td>
				</tr>
			</table>
		</div>	

</body>

</html>