--------------------------------------------------------

--
-- Table structure for table `customer_address`
--

CREATE TABLE `customer_address` (
  `id` int(20) NOT NULL,
  `customer_id` int(20) NOT NULL,
  `address_name` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `main` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_address`
--

INSERT INTO `customer_address` (`id`, `customer_id`, `address_name`, `type`, `main`) VALUES
(1, 1000, 'bucharest', 'billing', 0),
(2, 1001, 'sao paolo', 'billing', 0),
(3, 1000, 'madrid', 'shipping', 0),
(4, 1001, 'rome', 'shipping', 1),
(5, 1000, 'wein', 'billing', 0),
(6, 1001, 'london', 'billing', 0),
(7, 1001, 'lisbon', 'shipping', 0),
(8, 1002, 'iasi', 'billing', 0),
(9, 1001, 'paris', 'billing', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_address`
--
ALTER TABLE `customer_address`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_address`
--
ALTER TABLE `customer_address`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
